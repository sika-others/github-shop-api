#!/usr/bin/python

# BEGIN activacte virtualenv
try: execfile("env/bin/activate_this.py", dict(__file__="env/bin/activate_this.py"))
except IOError: pass
# END activacte virtualenv

import flask

import libgithubshop as lgs

app = flask.Flask(__name__)

@app.route("/")
def home():
    return "<h1>github shop api</h1><a href=\"http://ondrejsika.com\">Ondrej Sika</a>, <a href=\"http://ondrejsika.com/docs/github-shop-api\">documentation</a>"

@app.route("/api/items")
def api_items():
        return flask.Response(lgs.get_items_json(), mimetype="application/json")

if __name__ == '__main__':
    app.debug = True
    app.run()