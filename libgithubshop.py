import json

import requests
from pyquery import PyQuery as pq

def get_items():
    d = pq(requests.get("http://shop.github.com/").text)
    data = []
    for item in d(".item"):
        item = pq(item)
        name = item("h3").html()
        price = item(".price").html().replace("<span>$</span>", "")
        image_url = item("img").attr("src")
        detail_url = "http://shop.github.com"+item(".details").attr("href")
        in_stock = item(".footer").hasClass("in-stock")
        data.append({
            "name": name,
            "price": price,
            "image_url": image_url,
            "detail_url": detail_url,
            "in_stock": in_stock,
        })
    return data

def get_items_json():
    return json.dumps(get_items())