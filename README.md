GitHub Shop API
===============

### Authors
*  Ondrej Sika, <http://ondrejsika.com>, ondrej@ondrejsika.com

### Source
* GitHub: <https://github.com/sikaondrej/github-shop-api>


Documentation
-------------

### Instalation
Clone repo and run app.py: `python app.py`

In priduction mode via gunicorn (or uwsgi, ...)

    gunicorn wsgi:application -b 0.0.0.0:11230

### API
Currently is only one view.

#### Items
request

    GET /api/items

response

    [
        {
            "price": "25.00",
            "detail_url": "http://shop.github.com/products/contribution-graph-shirt",
            "image_url": "http://cdn.shopify.com/s/files/1/0051/4802/products/contribution-t_1024x1024.jpg?145",
            "name": "Contribution Graph Shirt",
            "in_stock": true
        },
        {
            "price": "25.00",
            "detail_url": "http://shop.github.com/products/arctocat",
            "image_url": "http://cdn.shopify.com/s/files/1/0051/4802/products/Actocat_1024x1024.png?145",
            "name": "Arctocat",
            "in_stock": true
        },
        ....
    ]